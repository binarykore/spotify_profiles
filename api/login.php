<?php
Require_Once($_SERVER["DOCUMENT_ROOT"]."/api/backend.php");
function login_redir($_composer = []){
	return("https://accounts.spotify.com/authorize?client_id=".($_composer["SPOTIFY_CLIENT_ID"])."&response_type=code&scope=user-read-currently-playing,user-read-recently-played&redirect_uri=".($_composer["BASE_URL"]."/callback"));
}
exit(Header("Location:".(login_redir(systemBackend()))));
?>