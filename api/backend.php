<?php
function systemBackend(){
	$_composer = parse_ini_file("spotify.ini");
	$_composer["BASE_URL"] = ("http://".($_SERVER["HTTP_HOST"]));
	$_composer["REDIRECT_URI"] = $_composer["BASE_URL"]."/callback";
	if(!empty(get_request_code())){
		$_composer["REQUEST_CODE"] = get_request_code();
	}elseif(empty(get_request_code())){
		$_composer["REQUEST_CODE"] = NULL;
	}
	return($_composer);
}
function get_request_code(){
	if(!empty($_GET["code"])){
		$_code = $_GET["code"];
	}elseif(empty($_GET["code"])){
		$_code = NULL;
	}
	return($_code);
}
function get_authorization($_composer){
	return(base64_encode($_composer["SPOTIFY_CLIENT_ID"].":".$_composer["SPOTIFY_SECRET_ID"]));
}
function generate_token($_composer){
	$_data = [];
	$_headers = ["Authorization: Basic ".(get_authorization($_composer))];
	$_data["grant_type"] = ("authorization_code");
	$_data["redirect_uri"] = ($_composer["REDIRECT_URI"]);
	$_data["code"] = ($_composer["REQUEST_CODE"]);
	$_data = http_build_query($_data);
	$_tokenfetcher = curl_init();
	curl_setopt($_tokenfetcher,CURLOPT_RETURNTRANSFER,true);
	curl_setopt($_tokenfetcher,CURLOPT_URL,$_composer["GENERATE_TOKEN"]);
	curl_setopt($_tokenfetcher,CURLOPT_FOLLOWLOCATION,true);
	curl_setopt($_tokenfetcher,CURLOPT_HEADER,false);
	curl_setopt($_tokenfetcher,CURLOPT_HTTPHEADER,$_headers);
	curl_setopt($_tokenfetcher,CURLOPT_POST,true);
	curl_setopt($_tokenfetcher,CURLOPT_POSTFIELDS,$_data);
	$_output = curl_exec($_tokenfetcher);
	curl_close($_tokenfetcher);
	if(!empty($_output)){
		$_datablob = ($_output);
	}elseif(empty($_output)){
		$_datablob = ["access_token"=>NULL];
	}	
	return($_datablob);
	//return(json_decode($_response,true));
	//Return as JSON
}
?>